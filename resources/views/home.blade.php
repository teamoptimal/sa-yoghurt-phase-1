<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>

        !-- Google Analytics -->
        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-123582189-2', 'auto');
        ga('send', 'pageview');
        </script>
        <!-- End Google Analytics -->

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-123582189-2"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-123582189-2');
        </script>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>The Second Yoghurt Summit</title>

        <!-- CSS -->
        <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- FONT AWESOME -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

        <link rel="stylesheet" href="{{asset('css/hamburgericonmenu.css')}}" />

        <link href="{{asset('css/hamburgers.css')}}" rel="stylesheet">

        <!-- Styles -->
        <style>
        </style>
    </head>
    <body>

        <div id="hamburgerMenuCustom">
            <nav>
                <ul>
                    <li><a href="/#register" onclick="closeMenu()">REGISTER</a></li>
                    <li><a href="/#speakers" onclick="closeMenu()">SPEAKERS</a></li>
                    <li><a href="/#programme" onclick="closeMenu()">PROGRAMME</a></li>
                    <li><a href="/#venue" onclick="closeMenu()">VENUE</a></li>
                    <li><a href="/#accomodation" onclick="closeMenu()">ACCOMODATION</a></li>
                    <li><a href="/#social" onclick="closeMenu()">SOCIAL FEED</a></li>
                </ul>
            </nav>
        </div>
    

        <!-- HAMBURGER MENU -->
        <button class="hamburger hamburger--spin " type="button">
            <span class="hamburger-box">
            <span class="hamburger-inner"></span>
            </span>
        </button>

        

    <div id="socialIcons">

        <a href="https://www.facebook.com/KnowYourYoghurt" target="_blank"><img class="menuSocialIcon" src="{{asset('images/Facebook.png')}}"></a>

        <a href="https://twitter.com/KnowYourYoghurt" target="_blank"><img class="menuSocialIcon" src="{{asset('images/twittericon.png')}}"></a>

        <!-- <a href=""><img class="menuSocialIcon" src="{{asset('images/Email.png')}}"></a> -->

    </div>

    <!-- INTRODUCTION -->
    <div id="register">

        <h1 class="hidden">The Second Yoghurt Summit.</h1>
        <h2 class="hidden">Realizing Children's Potential</h2>
        <div id="registerHeaderImage">
            <img src="{{asset('images/1.png')}}" alt="The Second Yoghurt Summit. Realizing Children's Potential.">
        </div>

        <h3 id="registerHeader">Taking an in-depth look at the realities and challenges of feeding the school going child in today's world.</h3>

        <div id="registerRegisterHere">

            <a href="https://www.eventbrite.com/e/the-second-yoghurt-summit-registration-47208098694?utm_source=eb_email&utm_medium=email&utm_campaign=order_confirmation_email&utm_term=eventname&ref=eemailordconf" target="_blank"><img src="{{asset('images/2.png')}}" alt="Register here before 10 AUgust 2018. Seats are limited"></a>

        </div>

    </div>

    <!-- SPEAKERS -->
    <div id="speakers">

        <div id="speakersPopUp">

            <i onclick="closePopUp()" class="fas fa-times" id="speakersPopUpClose"></i>

            <h4 id="speakersPopUpName">NAME</h4>

            <h5 id="speakersPopUpTitle">TITLE</h5>

            <h5 id="speakersPopUpLocation">LOCATION</h5>

            <h5 id="speakersPopUpText">TEXT</h5>

        </div>

        <h3 class="hidden">The Speakers</h3>

        <div id="speakersHeaderImage">

            <img src="{{asset('images/4.png')}}" alt="The Speakers">

        </div>

        <!-- PROFESSOR ANGELO TREMBLAY -->
        <div class="speaker">

            <h4 class="hidden">Professor Angelo Tremblay</h4>
            <h5 class="hidden">Professor at the Department of Kinesiology, Faculty of Medicine, laval University, Quebec</h5>
            <h6 class="hidden">Foods that support Metabolic Fitness</h6>
            <a onclick="popup('1')"> <img src="{{asset('images/5.png')}}" alt="Professor Angelo Tremblay"></a>

        </div>

        <!-- DR ANDRE MARETTE -->
        <div class="speaker">

            <h4 class="hidden">Dr Andre Marette</h4>
            <h5 class="hidden">Scientific Director of the Institute of Nutrition and Functional Foods, Laval University, Quebec</h5>
            <h6 class="hidden">Preventing Cardio Metabolic Diseases - How soon should we start?</h6>
            <a onclick="popup('2')"> <img src="{{asset('images/8.png')}}" alt="Dr Andre marette"></a>

        </div>

        <!-- LEBO MATSHEGO-RODA -->
        <div class="speaker">

            <h4 class="hidden">Lebo Matshego-Roda</h4>
            <h5 class="hidden">Nutritionist, Researcher and Post-Graduate Assitant - UNISA - Department of Life and Consumer Sciences, PhD candidate, Unisa</h5>
            <h6 class="hidden">Food as a means to realising children's growth potential</h6>
            <a onclick="popup('3')"><img src="{{asset('images/12.png')}}" alt="Lebo Matshego-Roda"></a>

        </div>

        <!-- NIKKI BUSH -->
        <div class="speaker">

            <h4 class="hidden">Nikki Bush</h4>
            <h5 class="hidden">Creative parenting expert, best-selling author and media personality</h5>
            <h6 class="hidden">Are moms overcompensating and using foods as an ally?</h6>
            <a onclick="popup('4')"><img src="{{asset('images/15.png')}}" alt="Nikki Bush"></a>

        </div>

    </div>

    <!-- PROGRAMME -->
    <div id="programme">

        <h3 class="hidden">Programme, 4 September 2018</h3>

        <div id="programmeImage">
            <a href="{{asset('pdf/programmepdf.pdf')}}" target="_blank"><img src="{{asset('images/programmeImage.png')}}"></a>
        </div>

        <!-- <table id="programmeTable">
            <tbody>
                <tr class="programmeRow">
                    <td class="programmeEventTime">13h30</td>
                    <td class="programmeEventName">REGISTRATION</td>
                    <td class="programmeEventDetails">Lion Room <br>Please take your seats and be part of out on-line poll</td>
                </tr>

                <tr class="programmeRow">
                    <td class="programmeEventTime">14h00</td>
                    <td class="programmeEventName">WELCOME</td>
                    <td class="programmeEventDetails"><span class="pigment">MONIQUE PIDERIT: </span>Programme Director: RD (SA), PhD Student, Department of Human Nutrition, University of Pretoria</td>
                </tr>

                <tr class="programmeRow">
                    <td class="programmeEventTime">14h05</td>
                    <td class="programmeEventName">OFFICIAL SUMMIT OPENING</td>
                    <td class="programmeEventDetails"><span class="pigment">HENDRIK BORN: </span>Managing Director, Danone Southern Africa</td>
                </tr>

                <tr class="programmeRow">
                    <td class="programmeEventTime">14h15</td>
                    <td class="programmeEventName">FOOD AS A MEANS TO REALISING CHILDREN's GROWTH POTENTIAL</td>
                    <td class="programmeEventDetails"><span class="pigment">LEBO MATSHEGO-RODA: </span>Nutritionist Researcher and Post-graduate Assistant - UNISA, Department of Life and Consumer Sciences, PhD candidate, UNISA</td>
                </tr>

                <tr class="programmeRow">
                    <td class="programmeEventTime">14h55</td>
                    <td class="programmeEventName">PREVENTING CARDIOMETABOLIC DISEASES - HOW SOON SHOULD BE START?</td>
                    <td class="programmeEventDetails"><span class="pigment">DR ANDRE MARETTE: </span>Professor of Medicine, Scientific Director, Institute of Nutrition & Functional Food, Laval University</td>
                </tr>

                <tr class="programmeRow">
                    <td class="programmeEventTime">15h30</td>
                    <td class="programmeEventName">TEA BREAK</td>
                    <td class="programmeEventDetails"><img id="rainbowImage" src="{{asset('images/rainbow.png')}}" alt="Rainbow Image"</td>
                </tr>

                <tr class="programmeRow">
                    <td class="programmeEventTime">16h00</td>
                    <td class="programmeEventName">FOODS THAT SUPPORT METABOLIC FITNESS</td>
                    <td class="programmeEventDetails"><span class="pigment">PROFESSOR ANGELO TREMBLAY: </span>Professor of the Departmnet of Kinesiology, Faculty of Medicine, Laval University</td>
                </tr>

                <tr class="programmeRow">
                    <td class="programmeEventTime">16h35</td>
                    <td class="programmeEventName">ARE MOMS OVERCOMPENSATING AND USING FOODS AS AN ALLY?</td>
                    <td class="programmeEventDetails"><span class="pigment">NIKKI BUSH: </span>Creative parenting expert, best sellign author and media personality</td>
                </tr>

                <tr class="programmeRow">
                    <td class="programmeEventTime">17h15</td>
                    <td class="programmeEventName">POST EVENT POLL</td>
                    <td class="programmeEventDetails">Please visit the Health Care Practitioners Wall and make your mark!</td>
                </tr>

                <tr class="programmeRow">
                    <td class="programmeEventTime">17h30 - 20h00</td>
                    <td class="programmeEventName">LIGHT SUPPER AND NETWORKING</td>
                    <td class="programmeEventDetails">The Playground</td>
                </tr>
            </tbody>
        </table> -->

        <div id="programmePrint">
            <a href="{{asset('pdf/programmepdf.pdf')}}" target="_blank"><img src="{{asset('images/21.png')}}" alt="Print Program"></a>
        </div> 

    </div>

    <!-- VENUE -->
    <div id="venue">

        <h3 class="hidden">Venue</h3>

        <div id="venueHeaderImage">

            <img src="{{asset('images/22.png')}}" alt="Venue">

        </div>

        <div id="venueDetails">

            <img src="{{asset('images/23.png')}}" alt="Venue Image">

            <div id="venueDetailsText">

                <table class="accomodationDetailsTextTable">
                    <tbody>
                        <tr>
                            <td class="accomodationDetailsIcon"></td>
                            <td><h4 class="colourLightBlue accomodationDetailsText1">MISTY HILLS COUNTRY HOTEL</h4></td>
                        </tr>
                        <tr>
                            <td class="accomodationDetailsIcon"><i class="fa fa-globe colourLightBlue" aria-hidden="true"></i></td>
                            <td><a href="http://mistyhills.co.za" target="_blank"><p class="colourLightBlue accomodationDetailsText2">http://mistyhills.co.za</p></a></td>
                        </tr>
                        <tr>
                            <td class="accomodationDetailsIcon"><i class="fa fa-envelope colourLightBlue" aria-hidden="true"></i></td>
                            <td><a href="mailto:sales@rali.co.za"><p class="colourLightBlue accomodationDetailsText3">sales@rali.co.za</p></a></td>
                        </tr>
                        <tr>
                            <td class="accomodationDetailsIcon"><i class="fa fa-mobile colourLightBlue" aria-hidden="true"></i></td>
                            <td><p class="colourLightBlue accomodationDetailsText4">+27 (11) 950 6000 | 0861 732 237</p></td>
                        </tr>
                        <tr>
                            <td class="accomodationDetailsIcon"><i class="fa fa-map-marker colourLightBlue" aria-hidden="true"></i></td>
                            <td><a href="https://goo.gl/maps/zBNKraz8WpE2" target="_blank"><p class="colourLightBlue accomodationDetailsText5">Muldersdrift Estate 69 Drift Boulevard (R114), Muldersdrift, Gauteng, South Africa</p></a></td>
                        </tr>
                    </tbody>
                </table>    

            </div>

            <div class="floatClear"></div>

        </div>

    </div>

    <!-- ACCOMODATION -->
    <div id="accomodation">

        <h3 class="hidden">Accomodation</h3>

        <div id="accomodationHeaderImage">

            <img src="{{asset('images/accommodation.png')}}" alt="Accomodation">

        </div>

        <!-- MONCHIQUE GUEST HOUSE -->
        <div class="accomodationDetails">
            <div class="accomodationDetailsImage">
                <img src="{{asset('images/24.png')}}" alt="Monchique Guest House">
            </div>
            <div class="accomodationDetailsText">
                <table class="accomodationDetailsTextTable">
                    <tbody>
                        <tr>
                            <td class="accomodationDetailsIcon"></td>
                            <td><h4 class="colourBlue accomodationDetailsText1">MONCHIQUE GUEST HOUSE</h4></td>
                        </tr>
                        <tr>
                            <td class="accomodationDetailsIcon"><i class="fa fa-globe colourBlue" aria-hidden="true"></i></td>
                            <td><a href="http://www.monchique.co.za/mc/" target="_blank"><p class="colourBlue accomodationDetailsText2">http://www.monchique.co.za/mc/</p></a></td>
                        </tr>
                        <tr>
                            <td class="accomodationDetailsIcon"><i class="fa fa-envelope colourBlue" aria-hidden="true"></i></td>
                            <td><a href="mailto:info@monchique.co.za"><p class="colourBlue accomodationDetailsText3">info@monchique.co.za</p></a></td>
                        </tr>
                        <tr>
                            <td class="accomodationDetailsIcon"><i class="fa fa-mobile colourBlue" aria-hidden="true"></i></td>
                            <td><p class="colourBlue accomodationDetailsText4">0825510851</p></td>
                        </tr>
                        <tr>
                            <td class="accomodationDetailsIcon"><i class="fa fa-map-marker colourBlue" aria-hidden="true"></i></td>
                            <td><a href="https://goo.gl/maps/BjBjoitmM2B2" target="_blank"><p class="colourBlue accomodationDetailsText5">Portion 3, Larsens Rd, Muldersdrift, Johannesburg, 1747</p></a></td>
                        </tr>
                    </tbody>
                </table>    
            </div>
        </div>

        <!-- RIVERSTONE LODGE -->
        <div class="accomodationDetails">
            <div class="accomodationDetailsImage">
                <img src="{{asset('images/26.png')}}" alt="Riverstone Lodhge">
            </div>
            <div class="accomodationDetailsText">
                <table class="accomodationDetailsTextTable">
                    <tbody>
                        <tr>
                            <td class="accomodationDetailsIcon"></td>
                            <td><h4 class="colourPink accomodationDetailsText1">RIVERSTONE LODHE</h4></td>
                        </tr>
                        <tr>
                            <td class="accomodationDetailsIcon"><i class="fa fa-globe colourPink" aria-hidden="true"></i></td>
                            <td><a href="http://www.riverstonelodge.co.za" target="_blank"><p class="colourPink accomodationDetailsText2">http://www.riverstonelodge.co.za</p></a></td>
                        </tr>
                        <tr>
                            <td class="accomodationDetailsIcon"><i class="fa fa-mobile colourPink" aria-hidden="true"></i></td>
                            <td><p class="colourPink accomodationDetailsText4">0116683111</p></td>
                        </tr>
                        <tr>
                            <td class="accomodationDetailsIcon"><i class="fa fa-map-marker colourPink" aria-hidden="true"></i></td>
                            <td><a href="https://goo.gl/maps/9xZUKe6AiP92" target="_blank"><p class="colourPink accomodationDetailsText5">N14, The Drift, Krugersdorp</p></a></td>
                        </tr>
                    </tbody>
                </table> 
            </div>
        </div>

        <!-- RANDOM HARVEST COUNTRY COTTAGES -->
        <div class="accomodationDetails">
            <div class="accomodationDetailsImage">
                <img src="{{asset('images/28.png')}}" alt="Random Harvest Country Cottages">
            </div>
            <div class="accomodationDetailsText">
                <table class="accomodationDetailsTextTable">
                    <tbody>
                        <tr>
                            <td class="accomodationDetailsIcon"></td>
                            <td><h4 class="colourGreen accomodationDetailsText1">RANDOM HARVEST COUNTRY COTTAGES</h4></td>
                        </tr>
                        <tr>
                            <td class="accomodationDetailsIcon"><i class="fa fa-globe colourGreen" aria-hidden="true"></i></td>
                            <td><a href="http://www.randomharvest.co.za" target="_blank"><p class="colourGreen accomodationDetailsText2">http://www.randomharvest.co.za/</p></a></td>
                        </tr>
                        <tr>
                            <td class="accomodationDetailsIcon"><i class="fa fa-envelope colourGreen" aria-hidden="true"></i></td>
                            <td><a href="mailto:cottages@rhq.co.za"><p class="colourGreen accomodationDetailsText3">cottages@rhq.co.za</p></a></td>
                        </tr>
                        <tr>
                            <td class="accomodationDetailsIcon"><i class="fa fa-mobile colourGreen" aria-hidden="true"></i></td>
                            <td><p class="colourGreen accomodationDetailsText4">072 562 3396</p></td>
                        </tr>
                        <tr>
                            <td class="accomodationDetailsIcon"><i class="fa fa-map-marker colourGreen" aria-hidden="true"></i></td>
                            <td><a href="https://goo.gl/maps/86ShTJ6tyuC2" target="_blank"><p class="colourGreen accomodationDetailsText5">Plot 57, Kontiki road, Muldersdrift, 1749</p></a></td>
                        </tr>
                    </tbody>
                </table>    
            </div>
        </div>

        <!-- AVIANTO HOTEL -->
        <div class="accomodationDetails">
            <div class="accomodationDetailsImage">
                <img src="{{asset('images/30.png')}}" alt="Avianto Hotel">
            </div>
            <div class="accomodationDetailsText">
                <table class="accomodationDetailsTextTable">
                    <tbody>
                        <tr>
                            <td class="accomodationDetailsIcon"></td>
                            <td><h4 class="colourLightBlue accomodationDetailsText1">AVIANTO HOTEL</h4></td>
                        </tr>
                        <tr>
                            <td class="accomodationDetailsIcon"><i class="fa fa-globe colourLightBlue" aria-hidden="true"></i></td>
                            <td><a href="https://www.avianto.co.za" target="_blank"><p class="colourLightBlue accomodationDetailsText2">https://www.avianto.co.za</p></a></td>
                        </tr>
                        <tr>
                            <td class="accomodationDetailsIcon"><i class="fa fa-envelope colourLightBlue" aria-hidden="true"></i></td>
                            <td><a href="mailto:info@avianto.co.za"><p class="colourLightBlue accomodationDetailsText3">info@avianto.co.za</p></a></td>
                        </tr>
                        <tr>
                            <td class="accomodationDetailsIcon"><i class="fa fa-mobile colourLightBlue" aria-hidden="true"></i></td>
                            <td><p class="colourLightBlue accomodationDetailsText4">0116683000</p></td>
                        </tr>
                        <tr>
                            <td class="accomodationDetailsIcon"><i class="fa fa-map-marker colourLightBlue" aria-hidden="true"></i></td>
                            <td><a href="https://goo.gl/maps/WCCKRiHC8fu" target="_blank"><p class="colourLightBlue accomodationDetailsText5">Plot 69, R114, Muldersdrift, Krugersdorp, 1747</p></a></td>
                        </tr>
                    </tbody>
                </table>    
            </div>
        </div>

        

    </div>

    <!-- SOCIAL -->
    <div id="social">

        <h3 class="hidden">Social Feed</h3>

        <div id="socialHeaderImage">

            <img src="{{asset('images/46.png')}}" alt="Social">

        </div>

        <script src="https://assets.juicer.io/embed.js" type="text/javascript"></script>
        <link href="https://assets.juicer.io/embed.css" media="all" rel="stylesheet" type="text/css" />
        <ul class="juicer-feed" data-feed-id="knowyouryoghurt" data-per="6" data-page="1" ><h1 class="referral"  style="display: none !important;"><a href="https://www.juicer.io">Powered by Juicer</a></h1></ul>
    </div>

    <div id="social">
        <div id="yoghurtMobile">
            <a href="https://sayoghurtinnutrition.co.za/" target="_blank"><img src="{{asset('images/footer.png')}}"></a>
        </div>
        <a href="https://sayoghurtinnutrition.co.za" target="_blank"><img id="socialWeb"src="{{asset('images/web.png')}}"></a>
        <a href="https://www.facebook.com/KnowYourYoghurt" target="_blank"><img id="socialFB" src="{{asset('images/fb.png')}}"></a>
        <a href="https://twitter.com/KnowYourYoghurt" target="_blank"><img id="socialTwitter" src="{{asset('images/twitter.png')}}"></a>
        <a href="https://www.danone.co.za/" target="_blank"><img id="socialDanone" src="{{asset('images/danone.png')}}"></a>
        <div class="floatClear"></div>
    </div>

    <!-- JQUERY -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <script>
        var $hamburger = $(".hamburger");
        $hamburger.on("click", function(e) {
            $hamburger.toggleClass("is-active");
            $(".hamburger-inner").toggleClass('hinner');
            $("#hamburgerMenuCustom").toggleClass('hamburgerShow');
            // Do something else, like open/close menu
        });
    </script>

    <script>

        function popup(popupNum) {

            $("#speakersPopUp").hide();
            
            switch(popupNum) {
                case "1":
                    $("#speakersPopUpClose").css('color', '#0055a5');
                    $("#speakersPopUpName").html("Biography - Angelo Tremblay").css('color', '#0055a5').show();
                    $("#speakersPopUpTitle").html("Professor at the Department of Kinesiology, Faculty of Medicine").css('color', '#0055a5').hide();
                    $("#speakersPopUpLocation").html("Laval University").css('color', '#0055a5').hide();
                    $("#speakersPopUpText").html("Professor Angelo Tremblay obtained his PhD in Physiology in Laval University, Quebec City, and is currently a professor in the Department of Kinesiology in this university. His investigations are mostly oriented towards the study of factors influencing energy balance in humans with the intent to improve obesity management. Recently, his research has been focused on the study of non-traditional determinants of obesity such as short sleep duration, low calcium/dairy intake, insufficient vitamin intake, suboptimal feeding behaviors, demanding cognitive effort and persistent organic pollutants. He is holder of the Canada Research Chair in Environment and Energy Balance. ").css('color', '#0055a5').show();
                    break;

                case "2":
                    $("#speakersPopUpClose").css('color', '#11938b');
                    $("#speakersPopUpName").html("Biography - ANDRE MARETTE, PhD").css('color', '#11938b').show();
                    $("#speakersPopUpTitle").html("Professor of Medicine <br> Scientific Director, Institute of  Nutrition & Functional Foods").css('color', '#11938b').show();
                    $("#speakersPopUpLocation").html("Laval University").css('color', '#11938b').show();
                    $("#speakersPopUpText").html("Dr. Marette is Professor of Medicine at the Heart and Lung Institute, Laval Hospital, and Scientific Director of the Institute of Nutrition and Functional Foods at Laval University.  He holds a research Chair on the pathogenesis of insulin resistance and cardiovascular diseases (CVD). Dr. Marette is an international renowned expert on the pathogenesis of insulin resistance and cardiometabolic diseases and his research has advanced the understanding of the physiological and molecular mechanisms of inflammation, and opened new possibilities for prevention and treatment and type 2 diabetes and CVD. He is also studying how nutrition and food ingredients can modulate the gut microbiota to protect against obesity-linked intestinal inflammation, fatty liver disease and type 2 diabetes.  He has published over 210 papers, reviews and book chapters and was invited to give more than a hundred lectures at various national & international conferences in the last 10 years. He currently serves as Editor-in-Chief for the Am J Physiol: Endo & Metab. and has authored two books in the last few years, one entitled La Vérité sur le Sucre edited by VLB and one entitled Yogurt: Roles in Nutrition and Impacts on Health, edited by CRC press. Dr. Marette has received several awards for his work including the prestigious Charles Best Award and Lectureship from the University of Toronto for his overall contribution to the advancement of scientific knowledge in the field of diabetes.").css('color', '#11938b').show();
                    break;

                case "3":
                    $("#speakersPopUpClose").css('color', '#ee2f6c');
                    $("#speakersPopUpName").html("Biography – Ms Lebogang Roda").css('color', '#ee2f6c').show();
                    $("#speakersPopUpTitle").html("3").css('color', '#ee2f6c').hide();
                    $("#speakersPopUpLocation").html("3").css('color', '#ee2f6c').hide();
                    $("#speakersPopUpText").html("Ms Lebogang Roda is a reasearcher and a post graduate assistant at the University of South Africa (UNISA) at the Department of Life & Consumer Sciences and is also studying for her PhD.  She is a member of the Nutrition Society of South Africa and a member of the Gauteng Province Task Team for the implementation of the Obesity Strategy in Gauteng Schools.  Her professional area of interest and expertise is in the area of nutrition education in relation to food choice behavior modification .").css('color', '#ee2f6c').show();
                    break;

                case "4":
                    $("#speakersPopUpClose").css('color', '#00853e');
                    $("#speakersPopUpName").html("Biography - Nikki Bush").css('color', '#00853e').show();
                    $("#speakersPopUpTitle").html("4").hide();
                    $("#speakersPopUpLocation").html("4").hide();
                    $("#speakersPopUpText").html("Creative parenting expert, Nikki Bush, has helped hundreds of thousands of parents to build fabulous relationships with their children by turning very ordinary, everyday moments into extraordinary memories. Nikki helps today’s busy parents to future-proof their children despite their busyness. Her wisdom, creativity and practical ideas are the solution for parents who are long on love and short on time. <br><br>Nikki’s work is fuelled by her passion for play, connection and relationships. She is a sought- after speaker and co-author of three bestselling books:  Future-proof Your Child (Penguin), Easy Answers to Awkward Questions (Metz Press) and Tech-Savvy Parenting (Bookstorm) and a number of ebooks. <br><br>She is the go to person for the media regarding anything to do with child development and parenting, racking up over 140 interviews a year including a weekly slot on SABC 3’s Expresso and Radio 702. She is a guest lecturer at Henley Business School, Wits Business School and GIBS. <br><br> Nikki lives in Johannesburg with her two sons.").css('color', '#00853e').show();
                    break;
            };

            $("#speakersPopUp").show();
        };

        function closePopUp() {
            if ($("#speakersPopUp").is(":visible")) {
                $("#speakersPopUp").hide();
            }
        };

        function closeMenu() {
            $hamburger.trigger("click");
        };

    </script>
       
    </body>
</html>
